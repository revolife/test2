var
    //modules
    gulp = require('gulp'),
    haml = require('gulp-haml'),
    slim = require("gulp-slim"),
    sass = require("gulp-sass"),
    coffee = require('gulp-coffee'),
    watch = require('gulp-watch'),

    //src path defines
    _srcDir = './src',
    _srcDirHaml = _srcDir + '/haml/**/*.haml',
    _srcDirSlim = _srcDir + '/slim/**/*.slim',
    _srcDirSass = _srcDir + '/sass/**/*.scss',
    _srcDirCoffee = _srcDir + '/coffee/**/*.coffee',

    //dist path defines
    _distDir = './dist',
    _distDirTemplates= _distDir + '/templates';
    _distDirStyles= _distDir + '/styles';
    _distDirScripts= _distDir + '/scripts';

gulp.task('haml', function () {
    gulp.src(_srcDirHaml)
        .pipe(haml())
        .pipe(gulp.dest(_distDirTemplates));
});

gulp.task('slim', function(){
    gulp.src(_srcDirSlim)
        .pipe(slim({
            pretty: true
        }))
        .pipe(gulp.dest(_distDirTemplates));
});

gulp.task('sass', function () {
    gulp.src(_srcDirSass)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(_distDirStyles));
});

gulp.task('coffee', function() {
    gulp.src(_srcDirCoffee)
        .pipe(coffee())
        .pipe(gulp.dest(_distDirScripts))
});

gulp.task('watch', function () {

    watch(_srcDirSass, function() {
        gulp.start('sass');
    });

    watch(_srcDirCoffee, function() {
        gulp.start('coffee');
    });

    watch(_srcDirHaml, function() {
        gulp.start('haml');
    });

    watch(_srcDirSlim, function() {
        gulp.start('slim');
    });

});

gulp.task('build-all', ['haml','slim','sass','coffee']);
gulp.task('default', ['build-all']);